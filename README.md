** Use .yml or spec-file.txt to create a conda env**

as explained there --> https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html

conda install --name [myenv] --file spec-file.txt

OR

conda env create -f environment.yml


then launch 

conda activate [myenv]

python just_3D_vis__compute_rho_grid_LVDB_L_VDB__case_L.py


---

