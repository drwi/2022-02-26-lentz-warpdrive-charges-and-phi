
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt



import matplotlib
from scipy.ndimage.interpolation import shift

from sympy.parsing.latex import parse_latex
from sympy.parsing.sympy_parser import parse_expr
import re
from suffix_tree import Tree
import operator
import os

import numexpr
import itertools
import editdistance

import json
from datetime import datetime
import time

import importlib



import open3d as o3d
import matplotlib.cm as cm


import matplotlib.pyplot as plt

from numpy.fft  import fft2, ifft2        
from scipy.interpolate import RegularGridInterpolator
from numpy import linspace, zeros, array

######################################################
def define_constants_grid(debug=False,max_over_sample_step=1):
    
    
    
    
    v_t=1.1e-16 # warp acceleration ####  9.8/(299792458)^2
    #9.8m/s-2 en m-1

    all_warp_accelerations={"1G":v_t,"0G":0,"-1G":-1*v_t}
    # we can only watch what happens with these

    # we use the sampling of van den broeck 0 to 3e-15 we can go further to 4e-15
    ####################

    

    
    A_p=1e-16
    A=A_p/max_over_sample_step
    
    B=51*A_p
    C=50*A_p
    
    

    
    
    # start stop step
    whole_X=list(np.arange(-C,B,A))

    print(whole_X)
    print("whole_X \n line 88 \n -- \n ")
    print(len(whole_X))
    print("LEN whole_X \n line 88 \n -- \n ")

    

    
    pos=list(np.arange(A,B,A))
    neg=-1*np.array(pos)
    if debug:
        print(neg)
        print("neg \n line 73 \n ---- \n ")
        print(pos)
        print("pos \n line 73 \n ---- \n ")
    
    
    X=whole_X
    if debug:
        print(X)
        print("X \n line 81 \n --- \n ")
    X.sort()
    rho=pos
    if debug:
        print(rho)
        print("rho \n line 81 \n --- \n ")
        input()
    step_rho=A
    step_X=A
    
    max_over_sample_step_phi=16.
    step_phi=np.pi/max_over_sample_step_phi
    
    phi=np.arange(-np.pi,np.pi+step_phi,step_phi)


    
    
    v=np.array([1e-5,5e-5,1e-4,4e-4,7e-4,1e-3,4e-3,7e-3,1e-2,4e-2,7e-2,1e-1,2.5e-1,5e-1,7.5e-1,0.9,0.99,0.999,0.9999,1.,1.1,1.5,2,2.5])
    return {'v':v,\
            'v_t':all_warp_accelerations,\
            "X":X,\
            'rho':rho,\
            'phi':phi,\
            "step_rho":step_rho,\
            "step_X":step_X,\
            "step_phi":step_phi,\
    }
######################################################

#################################################################################

#####################################################################

def create_one_rhomboid_rho(center_ampl={}, unit=1,prop_factor=1,total_widths=[],\
                            s_norm1_x_y=[],\
                            warp_speed_axis_coord=[],\
):
    
    X=warp_speed_axis_coord
    S=s_norm1_x_y

    center_couple=center_ampl["center"]
    amplitude=center_ampl["amplitude"]

    
    non_zero_places=(np.abs((X-center_couple[1]*1.0*unit)/(total_widths[1]*1.0*unit/2.0))+np.abs((S-center_couple[0]*1.0*unit)/(total_widths[0]*1.0*unit/2.0))<=1.0)
    non_zero_places_float=non_zero_places.astype(float)

    rho=amplitude*prop_factor*(np.abs((S-center_couple[0]*1.0*unit)/(total_widths[0]*1.0*unit/2.0))+1.0)*non_zero_places_float
    return {"rho":rho,\
            "non_zero_places":non_zero_places,\
    }

###########################################################
def imshow_plot(plane_x_z_reshaped):

    c=plt.imshow(plane_x_z_reshaped, cmap="hot")
    plt.colorbar(c)

    plt.title('y_ value plane_x_z_reshaped', 
              fontweight ="bold")
    plt.show()
    return

##########################################################
def     plot_x_z_of_quantity_cylindrical_coord(sum_rhos,conditional_numpy_array__for_y_eq_zero,phi_mesh):
    
    print('line 435')
    print(phi_mesh)
    print("phi_mesh \n line 437 \n --- \n ")
    
    
    if phi_mesh.shape[0]==0:
        print(phi_mesh)
        print("phi_mesh \n line 437 \n ---- \n ")
        print('error! \n line 449 \n --- \n')
        input()
    rhomboid_plane_x_z=sum_rhos[conditional_numpy_array__for_y_eq_zero]
    rhomboid_plane_x_z_reshaped=rhomboid_plane_x_z.reshape(phi_mesh.shape[0],phi_mesh.shape[1])
    #http://www.open3d.org/docs/0.9.0/tutorial/Basic/working_with_numpy.html
    #use_open3D_to_visualize_0()
    
    imshow_plot(rhomboid_plane_x_z_reshaped)
    return

##########################################################

##########################################################
def     plot_x_z_of_quantity(sum_rhos,conditional_numpy_array__for_y_eq_zero,sqrt_shape,phi_mesh):
    print('line 435')
    print(phi_mesh)
    print("phi_mesh \n line 437 \n --- \n ")
    
    
    if phi_mesh.shape[0]!=0:
        print(phi_mesh)
        print("phi_mesh \n line 437 \n ---- \n ")
        print(phi_mesh[conditional_numpy_array__for_y_eq_zero])
        print("phi_mesh[conditional_numpy_array__for_y_eq_zero] \n line 439 \n ---- \n ")
        
        input()
    rhomboid_plane_x_z=sum_rhos[conditional_numpy_array__for_y_eq_zero]
    rhomboid_plane_x_z_reshaped=rhomboid_plane_x_z.reshape(sqrt_shape,sqrt_shape)
    if phi_mesh.shape[0]!=0:
        imshow_plot(rhomboid_plane_x_z_reshaped)
    return
############################################################################################
def extract_non_zero_of_quantity(x_mesh=[],y_mesh=[],z_mesh=[],quantity=[]):
    if len(x_mesh)==0 or len(y_mesh)==0 or len(z_mesh)==0 or len(quantity)==0:
        print(len(x_mesh)==0 or len(y_mesh)==0 or len(z_mesh)==0 or len(quantity)==0)
        print('len(x_mesh)==0 or len(y_mesh)==0 or len(z_mesh)==0 or len(quantity)==0 \n line 493 \n ---- \n BIG PB !!')
        raise
    sum_rhos=quantity
    x_mesh_non_zro_sum_rhos=x_mesh[sum_rhos!=0]
    y_mesh_non_zro_sum_rhos=y_mesh[sum_rhos!=0]
    z_mesh_non_zro_sum_rhos=z_mesh[sum_rhos!=0]
    sum_rhos_non_zro_sum_rhos=sum_rhos[sum_rhos!=0]
    
    print(x_mesh_non_zro_sum_rhos.shape)
    print(y_mesh_non_zro_sum_rhos.shape)
    print(z_mesh_non_zro_sum_rhos.shape)
    print("z_mesh_non_zro_sum_rhos.shape \n line 848 \n -- \n ")

    
    xyz=[[x_mesh_non_zro_sum_rhos[a],\
          y_mesh_non_zro_sum_rhos[a],\
          z_mesh_non_zro_sum_rhos[a],\
          ]     for a in range(len(x_mesh_non_zro_sum_rhos))]
    return {"xyz":xyz,\
            "quantity":sum_rhos_non_zro_sum_rhos,\
    }

########################################################################
def compute_PHI_without_replication(inputs_cpwr): # negative indexing in the 2D convolution is understood as indexing fromm the end which causes replication before zero..
    PHI=inputs_cpwr["PHI"]
    z_mesh=inputs_cpwr["z_mesh"]
    x_mesh=inputs_cpwr["x_mesh"]
    y_mesh=inputs_cpwr["y_mesh"]
    debug=inputs_cpwr["debug"]
    heaviside_z_minus_x_o_vh=inputs_cpwr["heaviside_z_minus_x_o_vh"]
    sum_rhos_dot_signx=inputs_cpwr["sum_rhos_dot_signx"]
    
    v_h=inputs_cpwr["v_h"]

        
    
    if 0:
        # identify indices for x and z
        for k,v in {"x_mesh":x_mesh,\
                    "y_mesh":y_mesh,\
                    "z_mesh":z_mesh}.items():

            print(v[0,0,0])
            print(v[1,0,0])#y_mesh
            print(v[0,1,0]) # x_mesh
            print(v[0,0,1])#z_mesh
            print(k)
            print('line 779')
            input()

    dz=np.abs(z_mesh[0,0,1]-z_mesh[0,0,0])
    dx=np.abs(x_mesh[0,1,0]-x_mesh[0,0,0])

    print("dz--> "+str(dz))
    print("dx--> "+str(dx))
    print('line 552')
    
    z_mesh_shape=z_mesh.shape
    total_number_of_steps=z_mesh_shape[0]*z_mesh_shape[1]*z_mesh_shape[2]
    print(total_number_of_steps)
    if 0:
        for ind_y in range(z_mesh_shape[0]):
            print("str(ind_y)--->"+str(ind_y)+'///'+ str(z_mesh_shape[0]) +'\n line 784')
            for ind_x in range(z_mesh_shape[1]):
                for ind_z in range(z_mesh_shape[2]):

                    if np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z]):
                        print(np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z]))
                        print("np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z])")
                        print(str([ind_y,ind_x,ind_z]))
                        print("str([ind_y,ind_x,ind_z]) \n line 792 \n --- \n ")
                        input()

        print('line 795 \n --- \n success ')
        input()

    
    start_time = time.time()
    zs_1D=z_mesh[0,0,:]
    ys_1D=y_mesh[:,0,0]
    xs_1D=x_mesh[0,:,0]
    
    
    
    global_iterator=0
    for ind_y in range(z_mesh_shape[0]):
        print("str(ind_y)--->"+str(ind_y)+'///'+ str(z_mesh_shape[0]) +'\n line 566')
        if debug:
            if ind_y==3:
                break
        for ind_x in range(z_mesh_shape[1]):
            if ind_x%5==0:
                strx_y='y_ind='+str(ind_y)+'///'+str(z_mesh_shape[0])+'   '+\
                    'x_ind= '+str(ind_x)+'///'+str(z_mesh_shape[1])
                
                t1=time.time()
                t1_minu_t0=t1-start_time
                
                foreseen_tot_duration__of_task=t1_minu_t0*total_number_of_steps/(1.0*np.max([global_iterator,1]))
                time_remaining=foreseen_tot_duration__of_task-t1_minu_t0
                print(strx_y+"  globIter-->"+str(global_iterator)+'///'+str(total_number_of_steps)+"___   Elapsed (h)-->"+str(round(t1_minu_t0/3600.,2))+"   Remaining (h)-->"+str(round(time_remaining/3600.,2))+"  ///  Foreseen tot. duration (h) -->  "+str(round(foreseen_tot_duration__of_task/3600.,2))) 
                
            for ind_z in range(z_mesh_shape[2]):
                global_iterator+=1
                try:
                    


                    # convolution computation below

                    #
                    #PHI[ind_y,ind_x,ind_z]=np.sum([0 if (ind_x-ind_x_p<0 or ind_z-ind_z_p<0) else heaviside_z_minus_x_o_vh[ind_y,ind_x-ind_x_p,ind_z-ind_z_p]*sum_rhos_dot_signx[ind_y,ind_x_p,ind_z_p]*dz*dx for ind_x_p in range(z_mesh_shape[1]) for ind_z_p in range(z_mesh_shape[2])])

                    
                    PHI[ind_y,ind_x,ind_z]=np.sum([ 0.0  if zs_1D[ind_z]-zs_1D[ind_z_p]-np.abs((xs_1D[ind_x]-xs_1D[ind_x_p])/v_h)<=0 else \
                                                    sum_rhos_dot_signx[ind_y,ind_x_p,ind_z_p]*dz*dx \
                                                    for ind_x_p in range(z_mesh_shape[1]) for ind_z_p in range(z_mesh_shape[2])])

                    
                    
                except:
                    print(str([ind_y,ind_x,ind_z]))
                    print("str([ind_y,ind_x,ind_z]) \n line 590 \n --- \n ")

                    
                    raise
                    

                # this is a sum over two indices
    return {"PHI":PHI,\
            "zs_1D":zs_1D,\
            "ys_1D":ys_1D,\
            "xs_1D":xs_1D,\
    }
##########################################################
########################################################################
def compute_PHI_without_replication__pure_Lentz(inputs_cpwr): # negative indexing in the 2D convolution is understood as indexing fromm the end which causes replication before zero..
    print('pure lentz')
    
    PHI=inputs_cpwr["PHI"]
    z_mesh=inputs_cpwr["z_mesh"]
    x_mesh=inputs_cpwr["x_mesh"]
    y_mesh=inputs_cpwr["y_mesh"]
    debug=inputs_cpwr["debug"]
    heaviside_z_minus_x_o_vh=inputs_cpwr["heaviside_z_minus_x_o_vh"]
    sum_rhos_dot_signx=inputs_cpwr["sum_rhos_dot_signx"]
    
    v_h=inputs_cpwr["v_h"]
    sum_rhos=inputs_cpwr["sum_rhos"]

        
    
    if 0:
        # identify indices for x and z
        for k,v in {"x_mesh":x_mesh,\
                    "y_mesh":y_mesh,\
                    "z_mesh":z_mesh}.items():

            print(v[0,0,0])
            print(v[1,0,0])#y_mesh
            print(v[0,1,0]) # x_mesh
            print(v[0,0,1])#z_mesh
            print(k)
            print('line 779')
            input()

    dz=np.abs(z_mesh[0,0,1]-z_mesh[0,0,0])
    dx=np.abs(x_mesh[0,1,0]-x_mesh[0,0,0])

    print("dz--> "+str(dz))
    print("dx--> "+str(dx))
    print('line 552')
    
    z_mesh_shape=z_mesh.shape
    total_number_of_steps=z_mesh_shape[0]*z_mesh_shape[1]*z_mesh_shape[2]
    print(total_number_of_steps)
    if 0:
        for ind_y in range(z_mesh_shape[0]):
            print("str(ind_y)--->"+str(ind_y)+'///'+ str(z_mesh_shape[0]) +'\n line 784')
            for ind_x in range(z_mesh_shape[1]):
                for ind_z in range(z_mesh_shape[2]):

                    if np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z]):
                        print(np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z]))
                        print("np.isnan(heaviside_z_minus_x_o_vh[ind_y,ind_x,ind_z])")
                        print(str([ind_y,ind_x,ind_z]))
                        print("str([ind_y,ind_x,ind_z]) \n line 792 \n --- \n ")
                        input()

        print('line 795 \n --- \n success ')
        input()

    
    start_time = time.time()
    zs_1D=z_mesh[0,0,:]
    ys_1D=y_mesh[:,0,0]
    xs_1D=x_mesh[0,:,0]
    
    
    
    global_iterator=0
    for ind_y in range(z_mesh_shape[0]):
        print("str(ind_y)--->"+str(ind_y)+'///'+ str(z_mesh_shape[0]) +'\n line 566')
        if debug:
            if ind_y==3:
                break
        for ind_x in range(z_mesh_shape[1]):
            if ind_x%5==0:
                strx_y='y_ind='+str(ind_y)+'///'+str(z_mesh_shape[0])+'   '+\
                    'x_ind= '+str(ind_x)+'///'+str(z_mesh_shape[1])
                
                t1=time.time()
                t1_minu_t0=t1-start_time
                
                foreseen_tot_duration__of_task=t1_minu_t0*total_number_of_steps/(1.0*np.max([global_iterator,1]))
                time_remaining=foreseen_tot_duration__of_task-t1_minu_t0
                print(strx_y+"  globIter-->"+str(global_iterator)+'///'+str(total_number_of_steps)+"___   Elapsed (h)-->"+str(round(t1_minu_t0/3600.,2))+"   Remaining (h)-->"+str(round(time_remaining/3600.,2))+"  ///  Foreseen tot. duration (h) -->  "+str(round(foreseen_tot_duration__of_task/3600.,2))) 
                
            for ind_z in range(z_mesh_shape[2]):
                global_iterator+=1
                try:
                    
                    #
                    #PHI[ind_y,ind_x,ind_z]=np.sum([0 if (ind_x-ind_x_p<0 or ind_z-ind_z_p<0) else heaviside_z_minus_x_o_vh[ind_y,ind_x-ind_x_p,ind_z-ind_z_p]*sum_rhos_dot_signx[ind_y,ind_x_p,ind_z_p]*dz*dx for ind_x_p in range(z_mesh_shape[1]) for ind_z_p in range(z_mesh_shape[2])])
                    
                    # PHI[ind_y,ind_x,ind_z]=np.sum([ 0.0  if zs_1D[ind_z]-zs_1D[ind_z_p]-np.abs((xs_1D[ind_x]-xs_1D[ind_x_p])/v_h)<=0 else \
                    #                                 sum_rhos_dot_signx[ind_y,ind_x_p,ind_z_p]*dz*dx \
                    #                                 for ind_x_p in range(z_mesh_shape[1]) for ind_z_p in range(z_mesh_shape[2])])
                    PHI[ind_y,ind_x,ind_z]=np.sum([ 0.0  if zs_1D[ind_z]-zs_1D[ind_z_p]-np.abs((xs_1D[ind_x]-xs_1D[ind_x_p])/v_h)<=0 else \
                                                    sum_rhos[ind_y,ind_x_p,ind_z_p]*dz*dx \
                                                    for ind_x_p in range(z_mesh_shape[1]) for ind_z_p in range(z_mesh_shape[2])])


                    #sum_rhos
                    
                except:
                    print(str([ind_y,ind_x,ind_z]))
                    print("str([ind_y,ind_x,ind_z]) \n line 590 \n --- \n ")

                    
                    raise
                    

                # this is a sum over two indices
    return {"PHI":PHI,\
            "zs_1D":zs_1D,\
            "ys_1D":ys_1D,\
            "xs_1D":xs_1D,\
    }
##########################################################

##############################################################
def remake_lentz_rhomboid_charge(x_mesh,y_mesh,z_mesh,phi_mesh=np.array([]),cwd="",date_time_now_glob="",v_h=""):
    if len(str(v_h))==0:
        print(len(str(v_h))==0)
        print('len(str(v_h))==0 \n line 803 \n --- \n ')
        raise
    if len(cwd)==0:
        print("len(cwd)==0 \n line 494")
        raise
    if len(date_time_now_glob)==0:
        print('len(date_time_now_glob)==0 \n line 497 \n --- \n ')
        raise
    #z_mesh --> z is the soliton speed axis / the warp drive travel axis :)
    # we first need to compute abs(x)+abs(y)=s /// --> s_norm1_x_y
    s_norm1_x_y=np.abs(x_mesh)+np.abs(y_mesh)
    
    # warp drive axis is z_mesh
    unit=1e-15
    R=1*24
    

    conditional_numpy_array__for_y_eq_zero=(np.abs(y_mesh)<3e-30)
    print(phi_mesh.shape)
    print("phi_mesh.shape \n line 473 \n ---- \n ")
    
    if phi_mesh.shape[0]!=0:
        print(np.min(np.abs(phi_mesh)))
        print("np.min(np.abs(phi_mesh)) \n line 477 \n ---- \n ")
        

        min_abs_phi_mesh=np.min(np.abs(phi_mesh))
        input()
        conditional_numpy_array__for_phi_eq_zero=(np.abs(phi_mesh)==min_abs_phi_mesh)
        plane_x_z__from_phi=phi_mesh[conditional_numpy_array__for_phi_eq_zero]
        sqrt_shape__from_phi=int(np.sqrt(plane_x_z__from_phi.shape[0]))

        print(plane_x_z__from_phi.shape[0])
        print("plane_x_z__from_phi.shape[0] \n line 485 \n ---- \n ")

        print(sqrt_shape__from_phi)
        print("sqrt_shape__from_phi \n line 491 \n --- \n ")
        
        print(sqrt_shape__from_phi**2)
        print("sqrt_shape__from_phi**2  \n line 494 \n --- \n ")
        print(plane_x_z__from_phi.shape[0]==sqrt_shape__from_phi**2)
        print("plane_x_z__from_phi.shape[0]==sqrt_shape__from_phi**2 \n line 485 \n ---- \n ")
        
        plane_x_z_reshaped_from_phi=plane_x_z.reshape(sqrt_shape__from_phi,sqrt_shape__from_phi)

        input()
        print(plane_x_z_reshaped_from_phi)
        print("plane_x_z_reshaped_from_phi \n line 490 \n ---- \n ")
        input()
        
        
        
    if 1:
        #########################################################
        # extract the plane where y=0
        print(y_mesh==0)
        print("y_mesh==0 \n line 657 \n ---- \n ")

        print(np.min(np.abs(y_mesh)))
        print("np.min(np.abs(y_mesh)) \n line 454 \n ---- \n ")

        
        plane_x_z=y_mesh[np.abs(y_mesh)<3e-30]
        print(np.max(plane_x_z))
        print("np.max(plane_x_z) \n line455 \n --- \n ")
        print(np.min(plane_x_z))
        print("np.min(plane_x_z) \n line457 \n --- \n ")

        print(np.max(y_mesh))
        print("np.max(y_mesh) \n line460 \n --- \n ")
        print(np.min(y_mesh))
        print("np.min(y_mesh) \n line462 \n --- \n ")

        print(y_mesh)
        print("y_mesh \n line 469 \n ---\n ")
        print(x_mesh)
        print("x_mesh \n --- \n ")
        print(z_mesh)
        print("z_mesh \n line 473 \n --- \n ")
        #extract the y=0 plane.
        print(plane_x_z.shape)
        print("plane_x_z.shape\n line 476 \n --- \n ")

        sqrt_shape=int(np.sqrt(plane_x_z.shape[0]))
        print(plane_x_z.shape[0]==sqrt_shape**2)
        print("plane_x_z.shape[0]==sqrt_shape**2 \n line 480 \n ---- \n ")
        
        plane_x_z_reshaped=plane_x_z.reshape(sqrt_shape,sqrt_shape)
        print(plane_x_z)
        print("plane_x_z \n line 484 \n ---- \n ")
        print(plane_x_z_reshaped)
        print("plane_x_z_reshaped \n line 486 \n --- \n ")

        if 0:
            imshow_plot(plane_x_z_reshaped)

        
        #########################################################
    # initial lentz
    # centers_couples=[[1.,-1.],\
    #                  [-1.,-1.],\
    #                  [2.,0.],\
    #                  [-2.,0.],\
    #                  [1.,1.],\
    #                  [-1.,1.],\
    #                  [0.,2.],\
    #                  [0.,2.],\
    # ] #

    centers_amplitudes=[{"center":[2.,-1.],"amplitude":1},\
                        {"center":[3.,0.],"amplitude":-1},\
                        {"center":[2.,1.],"amplitude":-1},\
                        {"center":[0.,2.],"amplitude":2},\
    ] #

    #centers_couples
    
    #total_widths=[2.0,0.2] # initial LENTZ!
    #total_widths=[3.0,1.5]
    total_widths=[3.0,1.]
    #total_widths=[1.0,0.1]
    #total_widths=[.1,0.01]
    #total_widths=[.01,0.01]

    rhos=[]
    
    for center_ampl  in centers_amplitudes:
        res_rhomboid=create_one_rhomboid_rho(center_ampl, unit=unit,prop_factor=R,total_widths=total_widths,s_norm1_x_y=s_norm1_x_y,\
                                            warp_speed_axis_coord=z_mesh,\
                                            
        )

        rhomboid=res_rhomboid["rho"]
        non_zero_places=res_rhomboid["non_zero_places"]
        

        # let's do this!!

        print('line 532  \n ... \n .. \n .')
        
        # extract this plane
        # https://towardsdatascience.com/guide-to-real-time-visualisation-of-massive-3d-point-clouds-in-python-ea6f00241ee0
        
        rhos.append(rhomboid)

    print(np.array(rhos).shape)
    print("np.array(rhos).shape  \n line 559 \n ---- \n ")
    print(np.sum(rhos,axis=0).shape)
    print("np.sum(rhos).shape \n line 561 \n ---- \n ")
    sum_rhos=np.sum(rhos,axis=0)

    print("plot_x_z_of_quantity(sum_rhos,conditional_numpy_array__for_y_eq_zero,sqrt_shape) --> line 571 \n ---- \n ")

    
    if 1:
        # this computes the rho charge from lentz or something close :)
        plot_x_z_of_quantity(sum_rhos,conditional_numpy_array__for_y_eq_zero,sqrt_shape,phi_mesh)

    #############################
    

    print('line 673')
    

    
    
    for el in os.listdir(cwd):
        print(el)
    print('-------------')
    print(cwd)
    print("cwd \n line 863 \n ---- \n ")
    
          
    directory=cwd+'/'+"2022-01-29--open3D-plots"

    #import os

    path = directory
    

    # Check whether the specified path exists or not
    isExist = os.path.exists(path)

    if not isExist:

        # Create a new directory because it does not exist 
        os.makedirs(path)
        print("The new directory is created!")
    
    if 1: # this below works!
        res_enzoq=extract_non_zero_of_quantity(x_mesh=x_mesh,y_mesh=y_mesh,z_mesh=z_mesh,quantity=sum_rhos)
        plot_3D__with_open3D(res_enzoq=res_enzoq,directory=directory,date_time_now_glob=date_time_now_glob,txt_saving='cartesian_charge')

    
    
    sum_rhos_dot_signx=sum_rhos*np.sign(x_mesh)
    #sum_rhos_dot_signx__non_zro_sum_rhos=sum_rhos_dot_signx[sum_rhos!=0]
    
    if 0: # this below works!
        res_enzoq=extract_non_zero_of_quantity(x_mesh=x_mesh,y_mesh=y_mesh,z_mesh=z_mesh,quantity=sum_rhos_dot_signx)
        plot_3D__with_open3D(res_enzoq=res_enzoq,directory=directory,date_time_now_glob=date_time_now_glob,txt_saving="cartesian_charge_times_signx")

    
    ##########
    heaviside_z_minus_x_o_vh=np.heaviside(z_mesh-np.abs(x_mesh)/(1.0*v_h), 0.5)

    if 0:
        shape1=z_mesh.shape
        identity_3d = np.zeros(shape1)
        idx = np.arange(shape1[0])
        identity_3d[idx, idx, :] = 1  

        quantity_heaviside_modified=heaviside_z_minus_x_o_vh.astype('float')*1.1+identity_3d*1.0
        res_enzoq=extract_non_zero_of_quantity(x_mesh=x_mesh,y_mesh=y_mesh,z_mesh=z_mesh,quantity=quantity_heaviside_modified)
        plot_3D__with_open3D(res_enzoq=res_enzoq,directory=directory,date_time_now_glob=date_time_now_glob,txt_saving="heaviside_cartesian")

    #################################
    inputs_cpwr={}
    
    inputs_cpwr["z_mesh"]=z_mesh
    inputs_cpwr["x_mesh"]=x_mesh
    inputs_cpwr["y_mesh"]=y_mesh
    
    inputs_cpwr["debug"]=False
    inputs_cpwr["heaviside_z_minus_x_o_vh"]=heaviside_z_minus_x_o_vh
    inputs_cpwr["sum_rhos_dot_signx"]=sum_rhos_dot_signx
    inputs_cpwr["sum_rhos"]=sum_rhos
    inputs_cpwr["v_h"]=v_h

    
    outputs_toward_PHI_computation=inputs_cpwr
    return {"outputs_toward_PHI_computation":outputs_toward_PHI_computation}
####################################################################################"

def PHI_computation_cartesian(inputs_cpwr):
    z_mesh=inputs_cpwr["z_mesh"]
    x_mesh=inputs_cpwr["x_mesh"]
    y_mesh=inputs_cpwr["y_mesh"]
    cwd=inputs_cpwr["cwd"]
    date_time_now_glob=inputs_cpwr["date_time_now_glob"]
    
    #inputs_cpwr["debug"]=True
    inputs_cpwr["debug"]=False
    heaviside_z_minus_x_o_vh=inputs_cpwr["heaviside_z_minus_x_o_vh"]
    sum_rhos_dot_signx=inputs_cpwr["sum_rhos_dot_signx"]
    sum_rhos=inputs_cpwr["sum_rhos"]
    v_h=inputs_cpwr["v_h"]

    
    inputs_cpwr["z_mesh"]=z_mesh
    inputs_cpwr["x_mesh"]=x_mesh
    inputs_cpwr["y_mesh"]=y_mesh
    #inputs_cpwr["debug"]=True
    inputs_cpwr["debug"]=False
    inputs_cpwr["heaviside_z_minus_x_o_vh"]=heaviside_z_minus_x_o_vh
    inputs_cpwr["sum_rhos_dot_signx"]=sum_rhos_dot_signx
    inputs_cpwr["sum_rhos"]=sum_rhos
    inputs_cpwr["v_h"]=v_h


    


    #res_cpwr=compute_PHI_with_replication(inputs_cpwr) # negative indexing in the 2D convolution is understood as indexing fromm the end which causes replication before zero..
    PHI=np.zeros(z_mesh.shape)
    inputs_cpwr["PHI"]=PHI
    directory_save_large_np_array=cwd+'/'+"2022-02-05---save_large_numpy_arrays"


    #################
    directory=directory_save_large_np_array



    path = directory


    # Check whether the specified path exists or not
    isExist = os.path.exists(path)

    if not isExist:

        # Create a new directory because it does not exist 
        os.makedirs(path)
        print("The new directory is created!")
    ####
    ################
    

    if "pure_lentz" in list(inputs_cpwr.keys()):
        pure_lentz=inputs_cpwr["pure_lentz"]
    else:
        
        pure_lentz=True
    no_computation_PHI=True
    pure_lentz__str_saves_dict={True:'pure_lentz',\
                                False:'NOT_Lentz',\
    }
    str_save=pure_lentz__str_saves_dict[pure_lentz]
    if no_computation_PHI:
        #we search for the file
        # if we don't find the file, we compute the corresponding stuff
        try:
            list_dir_files_PHI_np_arrays=os.listdir(directory_save_large_np_array)
            list_dir_files_PHI_np_arrays=list(list_dir_files_PHI_np_arrays)
            list_dir_files_PHI_np_arrays.sort()
            for el in list_dir_files_PHI_np_arrays:
                print(el)
            print('line 1067')
            

            filename_PHI_1st_eval__found=[el for el in list_dir_files_PHI_np_arrays if "_PHI_first_numpy_array"+"_"+str_save+".npy" in el]
            if len(filename_PHI_1st_eval__found)==0:
                print('no correct file found!')
                print('pattern --> '+"_PHI_first_numpy_array"+"_"+str_save+".npy \n line 1081")
                print("in---"+directory_save_large_np_array)
                raise
            filename_PHI_1st_eval__found.sort()
            filename_PHI_1st_eval=directory_save_large_np_array+'/'+filename_PHI_1st_eval__found[-1]
            

            with open(filename_PHI_1st_eval, 'rb') as f:
        
                PHI_reloaded = np.load(f)

            zs_1D=z_mesh[0,0,:]
            ys_1D=y_mesh[:,0,0]
            xs_1D=x_mesh[0,:,0]
    

        except:
            #do_computation_nonetheless -->because we did not find the file!
            
            if pure_lentz:
                res_cpwr=compute_PHI_without_replication__pure_Lentz(inputs_cpwr) # negative indexing in the 2D convolution is understood as indexing fromm the end which causes replication before zero..
                

            else:
                res_cpwr=compute_PHI_without_replication(inputs_cpwr) # negative indexing in the 2D convolution is understood as indexing fromm the end which causes replication before zero..

            

    
            

    
            PHI=res_cpwr['PHI']
            
            
            zs_1D=res_cpwr["zs_1D"]
            ys_1D=res_cpwr["ys_1D"]
            xs_1D=res_cpwr["xs_1D"]
    
            filename_PHI_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_PHI_first_numpy_array"+"_"+str_save+".npy"

            filename_z_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_z_first_numpy_array"+"_"+str_save+".npy"
            filename_x_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_x_first_numpy_array"+"_"+str_save+".npy"
            filename_y_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_y_first_numpy_array"+"_"+str_save+".npy"

            dict_save_x_y_z_={"PHI":{"filename_saving":filename_PHI_1st_eval,\
                                     "array":PHI,\
            },\
                              "z":{"filename_saving":filename_z_1st_eval,\
                                   "array":z_mesh,\
            },\
                              "x":{"filename_saving":filename_x_1st_eval,\
                                   "array":x_mesh,\
            },\
                              "y":{"filename_saving":filename_y_1st_eval,\
                                   "array":y_mesh,\
            },\
            }
            for k in list(dict_save_x_y_z_.keys()):
                with open(dict_save_x_y_z_[k]["filename_saving"],'wb') as f :
                    np.save(f, dict_save_x_y_z_[k]["array"])
                with open(dict_save_x_y_z_[k]["filename_saving"], 'rb') as f:
                    if k!="PHI":
                        _reloaded = np.load(f)
                    else:
                        PHI_reloaded= np.load(f)

            PHI=0


    PHI=PHI_reloaded
    
    PHI_reloaded=0

    
    
    if 1: # this works well...
        print(' using -- > plot_3D__with_open3D \n -- \n ---- line 1214')
        res_enzoq=extract_non_zero_of_quantity(x_mesh=x_mesh,y_mesh=y_mesh,z_mesh=z_mesh,quantity=PHI)
        plot_3D__with_open3D(res_enzoq=res_enzoq,date_time_now_glob=date_time_now_glob,txt_saving="cartesian_PHI",cwd=cwd)

    res={"PHI_cart":PHI,\
         "x_mesh_cart":x_mesh,\
         "y_mesh_cart":y_mesh,\
         "z_mesh_cart":z_mesh,\
    }

    res["zs_1D"]=zs_1D
    res["ys_1D"]=ys_1D
    res["xs_1D"]=xs_1D
    res["str_save"]=str_save
    
    return res
##############################################
def from_cartesian_to_polar(inputs_fc2p):

    # we search for a file that seems correct
    
    cwd=inputs_fc2p["cwd"]
    date_time_now_glob=inputs_fc2p["date_time_now_glob"]
    str_save=inputs_fc2p["str_save"]    
    directory_save_large_np_array=cwd+'/'+"2022-02-05---save_large_numpy_arrays"

    no_computation=True
    if no_computation:
        files_directory_save_large_np_array=os.listdir(directory_save_large_np_array)
        
        filenames_PHI_1st_eval=[el for el in files_directory_save_large_np_array if "_PHIPolUnflat_PHI_2nd_npArray" in el]
        filenames_z_1st_eval=[el for el in files_directory_save_large_np_array if "_PHIPolUnflat_z_2nd_npArray" in el]
        filenames_x_1st_eval=[el for el in files_directory_save_large_np_array if "_PHIPolUnflat_x_2nd_npArray" in el]
        filenames_y_1st_eval=[el for el in files_directory_save_large_np_array if "_PHIPolUnflat_y_2nd_npArray" in el]

        if np.any([len(filenames_PHI_1st_eval)==0,\
                   len(filenames_z_1st_eval)==0,\
                   len(filenames_x_1st_eval)==0,\
                   len(filenames_y_1st_eval)==0]):
            print('files not found --> forced to redo computations line 1256')
            found_files=False
        else:
            found_files=True
            filenames_PHI_1st_eval.sort()
            filenames_z_1st_eval.sort()
            filenames_x_1st_eval.sort()
            filenames_y_1st_eval.sort()

            PHI_pol_filename=directory_save_large_np_array+'/'+filenames_PHI_1st_eval[-1]
            z_pol_filename=directory_save_large_np_array+'/'+filenames_z_1st_eval[-1]
            x_pol_filename=directory_save_large_np_array+'/'+filenames_x_1st_eval[-1]
            y_pol_filename=directory_save_large_np_array+'/'+filenames_y_1st_eval[-1]

            with open(PHI_pol_filename, 'rb') as f:
                unflattened_PHI_polar= np.load(f)

            with open(z_pol_filename, 'rb') as f:
                z_mesh_pol= np.load(f)
            with open(x_pol_filename, 'rb') as f:
                x_mesh_pol= np.load(f)
            with open(y_pol_filename, 'rb') as f:
                y_mesh_pol= np.load(f)



    if not found_files:
        print('line 1208')

        PHI_cart=inputs_fc2p["PHI_cart"]

        x_mesh_cart=inputs_fc2p["x_mesh_cart"]
        y_mesh_cart=inputs_fc2p["y_mesh_cart"]
        z_mesh_cart=inputs_fc2p["z_mesh_cart"]

        x_mesh_pol=inputs_fc2p["x_mesh_pol"]
        y_mesh_pol=inputs_fc2p["y_mesh_pol"]
        z_mesh_pol=inputs_fc2p["z_mesh_pol"]

        zs_1D=inputs_fc2p["zs_1D"]
        ys_1D=inputs_fc2p["ys_1D"]
        xs_1D=inputs_fc2p["xs_1D"]



        print('line 880 --> success!!!')

        # now we need to translate this into cylindro-polar coordinates


        # we check the unflatten solution!
        x_mesh_cart_flatten=x_mesh_cart.flatten()
        unflattenedx_mesh_cart=x_mesh_cart_flatten.reshape(x_mesh_cart.shape)
        print(np.all(unflattenedx_mesh_cart==x_mesh_cart))
        print("np.all(unflattenedx_mesh_cart==x_mesh_cart) \n line 1241 \n - --- \n ")

        y_mesh_cart_flatten=y_mesh_cart.flatten()
        unflattenedy_mesh_cart=y_mesh_cart_flatten.reshape(x_mesh_cart.shape)
        print(np.all(unflattenedy_mesh_cart==y_mesh_cart))
        print("np.all(unflattenedy_mesh_cart==y_mesh_cart) \n line 1246 \n - --- \n ")

        z_mesh_cart_flatten=z_mesh_cart.flatten()
        unflattenedz_mesh_cart=z_mesh_cart_flatten.reshape(x_mesh_cart.shape)
        print(np.all(unflattenedz_mesh_cart==z_mesh_cart))
        print("np.all(unflattenedz_mesh_cart==z_mesh_cart) \n line 1251 \n - --- \n ")

        print('----------------- \n line 1253 \n -- \n ')
        print(type(x_mesh_pol))
        print("type(x_mesh_pol) \n line 1283 \n --- \n ")

        x_mesh_pol_flatten=x_mesh_pol.flatten()
        print(type(x_mesh_pol))
        print("type(x_mesh_pol) \n line 1284 \n --- \n ")
        
        unflattenedx_mesh_pol=x_mesh_pol_flatten.reshape(x_mesh_pol.shape)
        print(np.all(unflattenedx_mesh_pol==x_mesh_pol))
        print("np.all(unflattenedx_mesh_pol==x_mesh_pol) \n line 1257 \n - --- \n ")

        y_mesh_pol_flatten=y_mesh_pol.flatten()
        unflattenedy_mesh_pol=y_mesh_pol_flatten.reshape(x_mesh_pol.shape)
        print(np.all(unflattenedy_mesh_pol==y_mesh_pol))
        print("np.all(unflattenedy_mesh_pol==y_mesh_pol) \n line 1262 \n - --- \n ")

        z_mesh_pol_flatten=z_mesh_pol.flatten()
        unflattenedz_mesh_pol=z_mesh_pol_flatten.reshape(x_mesh_pol.shape)
        print(np.all(unflattenedz_mesh_pol==z_mesh_pol))
        print("np.all(unflattenedz_mesh_pol==z_mesh_pol) \n line 1267 \n - --- \n ")
        

        #############################

        
        fn = RegularGridInterpolator((ys_1D,xs_1D,zs_1D), PHI_cart)
        pts=list(zip(y_mesh_pol_flatten,x_mesh_pol_flatten,z_mesh_pol_flatten))


        print(len(z_mesh_pol_flatten))
        print("len(z_mesh_pol_flatten) \n line 1319 \n --- \n ")
        print(len(x_mesh_pol_flatten))
        print("len(x_mesh_pol_flatten) \n line 1321 \n --- \n ")

        print(len(y_mesh_pol_flatten))
        print("len(y_mesh_pol_flatten) \n line 1324 \n --- \n ")

        
        print('line 1318')
        print(len(pts))
        print("len(pts) \n line 1319 \n ------ \n ")

        PHI_polar=[]
        start_time=time.time()
        total_number_of_steps=len(pts)

        print(type(x_mesh_pol))
        print("type(x_mesh_pol) \n line 1346 \n --- \n ")
        print(x_mesh_pol.shape)
        print("x_mesh_pol.shape \n line 1348 \n --- \n ")
        

        for i_ in range(len(pts)):
            if i_%1000==0:
                strx_y='i_='+str(i_)+'///'+str(len(pts)-1)+'   '
                t1=time.time()
                t1_minu_t0=t1-start_time
                
                foreseen_tot_duration__of_task=t1_minu_t0*total_number_of_steps/(1.0*np.max([i_,1]))
                time_remaining=foreseen_tot_duration__of_task-t1_minu_t0
                
                print(strx_y+"  ///"+str(total_number_of_steps)+"___   Elapsed (min)-->"+str(round(t1_minu_t0/60.,2))+"   Remaining (min)-->"+str(round(time_remaining/60.,2))+"  ///  Foreseen tot. duration (min) -->  "+str(round(foreseen_tot_duration__of_task/60.,2))) 

            phi_value=fn(pts[i_])
            PHI_polar.append(phi_value)
        print("line 1338 --> success?!!..!??")
        print(len(PHI_polar))
        print("len(PHI_polar) \n line 1340")
        print(np.all(np.array(PHI_polar)==0))
        print("np.all(np.array(PHI_polar)==0) \n line 1381 \n ---- \n ")

        print(type(x_mesh_pol))
        print("type(x_mesh_pol) \n line 1430 \n --- \n ")
        print(x_mesh_pol.shape)
        print("x_mesh_pol.shape \n line 1432 \n --- \n ")
        

        PHI_polar=np.array(PHI_polar)
        unflattened_PHI_polar=PHI_polar.reshape(x_mesh_pol.shape)


        #################################


        

        
        filename_PHI_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_PHIPolUnflat_PHI_2nd_npArray"+"_"+str_save+".npy"

        filename_z_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_PHIPolUnflat_z_2nd_npArray"+"_"+str_save+".npy"
        filename_x_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_PHIPolUnflat_x_2nd_npArray"+"_"+str_save+".npy"
        filename_y_1st_eval=directory_save_large_np_array+'/'+date_time_now_glob+"_PHIPolUnflat_y_2nd_npArray"+"_"+str_save+".npy"

        dict_save_x_y_z_={"PHI":{"filename_saving":filename_PHI_1st_eval,\
                                 "array":unflattened_PHI_polar,\
        },\
                          "z":{"filename_saving":filename_z_1st_eval,\
                               "array":z_mesh_pol,\
        },\
                          "x":{"filename_saving":filename_x_1st_eval,\
                               "array":x_mesh_pol,\
        },\
                          "y":{"filename_saving":filename_y_1st_eval,\
                               "array":y_mesh_pol,\
        },\
        }
        


        for k in list(dict_save_x_y_z_.keys()):
            with open(dict_save_x_y_z_[k]["filename_saving"],'wb') as f :
                np.save(f, dict_save_x_y_z_[k]["array"])
            with open(dict_save_x_y_z_[k]["filename_saving"], 'rb') as f:
                if k!="PHI":
                    _reloaded = np.load(f)
                else:
                    unflattened_PHI_polar= np.load(f)




        
        #################################
    return {"unflattened_PHI_polar":unflattened_PHI_polar,\
            "z_mesh_pol":z_mesh_pol,\
            "x_mesh_pol":x_mesh_pol,\
            "y_mesh_pol":y_mesh_pol,\
    }
###########################################################


def plot_3D__with_open3D(res_enzoq={},directory="",date_time_now_glob="",txt_saving='',cwd=""):
    if len(res_enzoq)==0:
        print(len(res_enzoq))
        print('line 830 --> big pb')
        raise
    xyz=res_enzoq["xyz"]
    quantity=res_enzoq["quantity"]
    if len(directory)==0:
        print(len(directory)==0)
        print("len(directory)==0 \n line 682 \n --- \n ")
        directory=cwd+'/'+"2022-01-29--open3D-plots"
        
    if len(quantity)==0:
        print(len(quantity)==0)
        print("len(quantity)==0 \n line 686 \n --- \n ")
        raise
    if len(cwd)==0:
        print(len(cwd)==0)
        print("len(cwd)==0 \n line 1460 \n --- \n ")
        if len(directory)==0:
            print('line 1462 --> pb:!')
            raise

    if len(xyz)==0:
        print(len(xyz)==0)
        print("len(xyz)==0 \n line 690 \n --- \n ")
        raise
    if len(date_time_now_glob)==0:
        print("len(date_time_now_glob)==0")
        print(date_time_now_glob)
        print("date_time_now_glob \n line 694 \n --- \n ")
        raise
        
    
    sum_rhos_non_zro_sum_rhos=quantity

    # Pass xyz to Open3D.o3d.geometry.PointCloud and visualize
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)


    colors = sum_rhos_non_zro_sum_rhos
    #https://github.com/isl-org/Open3D/issues/614
    #https://www.programcreek.com/python/example/110517/open3d.Vector3dVector
    min_=np.min(sum_rhos_non_zro_sum_rhos)
    max_=np.max(sum_rhos_non_zro_sum_rhos)

    print(min_)
    print("min_ \n line 862 \n --- \n ")

    print(max_)
    print("max_ \n line 865 \n -- \n ")

    colors = cm.gist_rainbow( (sum_rhos_non_zro_sum_rhos - min_) / (max_ - min_) )[:, 0:3]

    # add colors to visualization
    pcd.colors = o3d.utility.Vector3dVector( colors )
    
    list_dist=[(np.sqrt(np.sum((np.array(xyz[0])-np.array(xyz[a]))**2))) for a in range(len(xyz))]
    list_dist_non_zro=[el for el in list_dist if el!=0]

    xyz_smallest_distance_from_0th_point=np.min(list_dist_non_zro)
    xyz_average_distance_from_0th_point=np.mean(list_dist_non_zro)
    
    print(xyz_smallest_distance_from_0th_point)
    print("xyz_smallest_distance_from_0th_point \n line 733\n ---- \n ")
    
    #http://www.open3d.org/docs/release/python_api/open3d.geometry.TriangleMesh.html#open3d.geometry.TriangleMesh.create_coordinate_frame
    #The x, y, z axis will be rendered as red, green, and blue arrows respectively.
    mesh_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(size=xyz_average_distance_from_0th_point,origin=[0.,0.,0.])
    ############
    filename_sync_ply=directory+'/'+date_time_now_glob+"_sync.ply"

    o3d.io.write_point_cloud(filename_sync_ply, pcd)
    
    # Load saved point cloud and visualize it
    pcd_load = o3d.io.read_point_cloud(filename_sync_ply)
    o3d.visualization.draw_geometries([pcd_load,mesh_frame])

    # convert Open3D.o3d.geometry.PointCloud to numpy array
    xyz_load = np.asarray(pcd_load.points)
    print('xyz_load')
    print(xyz_load)
    ####################################
    #### colorbar saving
    
    sum_rhos_non_zro_sum_rhos_flatten = sum_rhos_non_zro_sum_rhos.flatten()


    start=min_
    stop=max_
    x = np.linspace(start, stop, num=50) 
    y = x
    
    #sum_rhos_non_zro_sum_rhos_flatten
    colors=cm.gist_rainbow((x  - min_) / (max_ - min_) )
    fig, ax = plt.subplots()
    plt.scatter(x, y, s=15, c=colors, alpha=0.5)
    plt.savefig(directory+'/'+date_time_now_glob+'_'+txt_saving+"_test_cmap.png", dpi=150)
    
    plt.close('all')
    return
##############################################################################
##########################################################

###########################################################
def main():
    iterator_output_py=0
    now = datetime.now()
    
    timestamp = datetime.timestamp(now)
    date_time_now_glob = now.strftime("%Y-%m-%d--%H-%M-%S")
    cwd=os.getcwd()
    
    #####################
    
    #####################################
    print('-------')
    
    ###############################################################################
    #result_dict=define_constants_grid(debug=True)
    result_dict=define_constants_grid(debug=False)

    # the results of this function as of 2021-12-23 are :
    #######
    #'v' --> warp speeds
    #'v_t'--> warp accelerations
    #"X" --> the X positions in the grid (on the polar axis)
    #'rho' --> the radial distance (rho) of the cylindrical coordinate
    #'phi':angular position , i.e. azimuth
    # "step_rho" --> constant steps of rho 
    # "step_X" --> constant steps of X
    # "step_phi"--> constant step of phi
    #######


    
    #############
    list_keys_rd=list(result_dict.keys())
    list_keys_rd.sort()
    debug=True
    if debug:
        for el in list_keys_rd:
        
            print(el)
    print('\n ------ \n line 1076 \n ------ \n make easy expression and see if it works ? ')

    step_rho=result_dict["step_rho"]
    step_X=result_dict["step_X"]
    step_phi=result_dict["step_phi"]
    
    v=result_dict['v']
    v_t=result_dict['v_t']
    X=result_dict["X"]
    rho=result_dict['rho']
    phi=result_dict['phi']

    print(len(X))
    print("LEN X")
    
    print(len(rho))
    print("LEN rho")
    print(len(phi))
    print("LEN phi")
    print('\n line 2493 \n --- \n ')
    
    print("LENS--> X,rho,phi")

    
    # this order below should not change,
    #if it does change the order of each gradient is different... derivative ordering changes... --> a mess in disguise would be created!
    X_mesh,rho_mesh,phi_mesh=np.meshgrid(X,rho,phi)
    print(phi_mesh.shape)
    print("phi_mesh.shape \n line 836 \n ---- \n ")

    

    v_h=4.0
    # this below is just for fun.. heu.. for trials / try outs / attempts to remake lentz graph
    if 1:
        x_mesh,y_mesh,z_mesh=np.meshgrid(X,X,X)

        # this is cartesian coordinates!
        results_rlrc=remake_lentz_rhomboid_charge(x_mesh,y_mesh,z_mesh,cwd=cwd,date_time_now_glob=date_time_now_glob,v_h=v_h)

        inputs_PHI_cpcr=results_rlrc["outputs_toward_PHI_computation"]
        
        inputs_PHI_cpcr["cwd"]=cwd
        inputs_PHI_cpcr["date_time_now_glob"]=date_time_now_glob

        inputs_PHI_cpcr["pure_lentz"]=True
        res_PHI_cpcr=PHI_computation_cartesian(inputs_PHI_cpcr)

        z_mesh_pol=X_mesh
        y_mesh_pol=rho_mesh*np.cos(phi_mesh)
        x_mesh_pol=rho_mesh*np.sin(phi_mesh)

        print(type(x_mesh_pol))
        print("type(x_mesh_pol) \n line 2109 \n ---- \n ")
        print(x_mesh_pol.shape)
        print("x_mesh_pol.shape \n line 2111 \n ---- \n ")


        res_PHI_cpcr["x_mesh_pol"]=x_mesh_pol
        res_PHI_cpcr["y_mesh_pol"]=y_mesh_pol
        res_PHI_cpcr["z_mesh_pol"]=z_mesh_pol


        res_PHI_cpcr["cwd"]=cwd
        res_PHI_cpcr["date_time_now_glob"]=date_time_now_glob

        
        res_fc2p=from_cartesian_to_polar(res_PHI_cpcr)

        unflattened_PHI_polar=res_fc2p["unflattened_PHI_polar"]

        # plot polar and cartesian and try to make sense with it :)
        print('line 2301') 
        if 1: # this works well?... YES (resounding)
            res_enzoq=extract_non_zero_of_quantity(x_mesh=x_mesh_pol,y_mesh=y_mesh_pol,z_mesh=z_mesh_pol,quantity=unflattened_PHI_polar)
            plot_3D__with_open3D(res_enzoq=res_enzoq,date_time_now_glob=date_time_now_glob,txt_saving="polar_PHI",cwd=cwd)

        # this works! above
        #######################
    if 0: # useless
        # so we do the same below but we replac x_mesh and y_mesh and z_mesh by the correct stuff..

        z_mesh=X_mesh
        y_mesh=rho_mesh*np.cos(phi_mesh)
        x_mesh=rho_mesh*np.sin(phi_mesh)

        print('line 832')
        
        results_rlrc2=remake_lentz_rhomboid_charge__cylindrical_coordinates(x_mesh,y_mesh,z_mesh,phi_mesh=phi_mesh,cwd=cwd,date_time_now_glob=date_time_now_glob)
        
        print('lin 835')
    return
###################################
    
if __name__ == "__main__":
    # execute only if run as a script
    
    main()
